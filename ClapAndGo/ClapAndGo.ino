
#define LED 13
#define MICRO 7
#define RELAY 8
#define TEMPSCLAP 500
#define TEMPSENTRE2CLAP 1000



void setup() {
  Serial.begin(9660);
  pinMode(LED, OUTPUT);
  pinMode(MICRO, INPUT);
  pinMode(RELAY, OUTPUT);
  digitalWrite(LED, LOW);
  digitalWrite(RELAY, LOW);
  
}

// the loop function runs over and over again forever
void loop() {
  static bool Memoire=LOW;
  
 // if(digitalRead(MICRO)== LOW)
  if(Detect2Clap()==true)
  {
    Memoire = !Memoire;//changement d'état de la LED.
    digitalWrite(LED, Memoire);
    digitalWrite(RELAY, Memoire);
    delay(400); //après un changement d'etat on bloque le systeme 0.4 seconde.
  }  
}

bool DetectClap()
{
  int i=0;// mesure du temps d'un clap
  bool clap = false;
  
    if(digitalRead(MICRO)==LOW)// début de clap détecté
    {
      while(digitalRead(MICRO)==LOW && i<TEMPSCLAP)
      {
      delay(1);
      i=i+1;
      }
      if(i<TEMPSCLAP)
      {
        clap = true;
        Serial.println(i);
        Serial.println(" =>duree d'un clap\n");
        i=0;
      }
      else
      {
        Serial.println("Pas clap ! \n");
        Serial.println(i);
        Serial.println(" =>duree d'un clap\n");
        i=0;
      }
    }
  return clap;
}

bool Detect2Clap()
{
  int j;// mesure du temps entre 2 claps.
  bool double_clap = false;
  bool clap=DetectClap();
  
    if(clap==true)
    {
      Serial.println("Premier clap ! \n");
      delay(1);
      clap=DetectClap();
      j=0;
      while(clap==false && j < TEMPSENTRE2CLAP)
      {
      clap=DetectClap();
      j=j+1;
      delay(1);
      }
      if(clap==true && j<TEMPSENTRE2CLAP)
      {
        double_clap=true;
        Serial.println("Double clap ! \n");
        Serial.println(j);
        Serial.println("j avant boucle \n");
      }
      else
      {
        Serial.println("Pas double clap ! \n");
      }
    }
  return double_clap;
}


